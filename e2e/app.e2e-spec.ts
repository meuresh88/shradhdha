import { ShradhdhaPage } from './app.po';

describe('shradhdha App', function() {
  let page: ShradhdhaPage;

  beforeEach(() => {
    page = new ShradhdhaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
