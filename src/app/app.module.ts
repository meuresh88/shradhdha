import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {RouterModule} from "@angular/router";
import {ListViewComponent} from "./Components/list-view/list-view.component";
import {AddEditComponent} from "./Components/add-edit/add-edit.component";
import {MainParentComponent} from "./Components/main-parent/main-parent.component";
import {MainStudentComponent} from "./Components/main-student/main-student.component";
import {SearchControlViewComponent} from "./Components/search-control-view/search-control-view.component";
import {MainComponent} from "./Components/main/main.component";
/**
 * Created by uresh on 8/29/16.
 */
export const URLS = {
  parentView:"parent",
  studentView:"student",
  home:"home",
  add:"add/:isParent",
  edit:"edit/:regNo/:isParent",
};

@NgModule({
  declarations: [
    AppComponent,
    AddEditComponent,
    MainParentComponent,
    MainStudentComponent,
    SearchControlViewComponent,
    ListViewComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/'+URLS.home,
        pathMatch: 'full'
      },
      {
        path: URLS.home,
        component: MainComponent
      },
      {
        path: URLS.edit,
        component:AddEditComponent
      },
      {
        path: URLS.add,
        component:AddEditComponent
      },
      {
        path: URLS.parentView,
        component: MainParentComponent
      },

      {
        path: URLS.studentView,
        component: MainStudentComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

