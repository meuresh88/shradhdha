import { Component, OnInit } from '@angular/core';
import {Student} from "../../Objects/Student";
import {ListPageData} from "../../Objects/ListPageData";
import {SearchControlViewComponent} from "../search-control-view/search-control-view.component";
import {ListViewComponent} from "../list-view/list-view.component";

@Component({
  selector: 'app-main-student',
  templateUrl: 'main-student.component.html',
  styleUrls: ['main-student.component.css'],
})
export class MainStudentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.tempbDay = new Date("1988-12-09");
    this.tempParent   = {name:"Sunil",address:"844/5,Nawala,Gampola",phone:"0718266496",regNum:"dupmRegNo",accNumber:"018200150003876",school:"Vidyartha College",bday:this.tempbDay}
    this.parentList = [this.tempParent];
    this.tempPageData= {list:this.parentList,currentPageNumber:1,maxPageNumber:1};
  }

  tempPageData:ListPageData;
  tempParent:Student;
  tempbDay:Date;
  parentList:any[];


}
