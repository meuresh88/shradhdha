import { Component, OnInit ,OnDestroy} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {ListPageData} from "../../Objects/ListPageData";
import {ListViewComponent} from "../list-view/list-view.component";

@Component({
  selector: 'add-edit',
  templateUrl: 'add-edit.component.html',
  styleUrls: ['add-edit.component.css']
})
export class AddEditComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }


  ngOnInit() {
    this.routePara = this.route.params.subscribe(params => {
      this.personRegNo = params['regNo'];
      this.isParent = <boolean>(params['isParent'] == "true");
      this.item.regNum= params['regNo'];
    });

    // if(this.lastSelectedObject){
    //   this.item= this.lastSelectedObject;
    // }

    this.item.address = {}; //TODO: remove this with the implemantation
    this.tempbDay = new Date("1988-12-09");
    if(!this.isParent){
      this.tempParent   = {name:"Sunil",address:"844/5,Nawala,Gampola",ammount:4500,email:"temp@gmail.com",phone:"0718266496",regNum:"dupmRegNo"}
    }else {
      this.tempParent   = {name:"Sunil",address:"844/5,Nawala,Gampola",phone:"0718266496",regNum:"dupmRegNo",accNumber:"018200150003876",school:"Vidyartha College",bday:this.tempbDay}
    }
    this.parentList = [this.tempParent];
    this.tempPageData= {list:this.parentList,currentPageNumber:1,maxPageNumber:1};
  }

  tempPageData:ListPageData;
  tempParent:any;
  tempbDay:Date;
  parentList:any[];


  ngOnDestroy(){
    this.routePara.unsubscribe();
  }

  routePara:any;
  personRegNo:any;
  isParent:boolean;
  item:any={};


  addEdit(detail){
    console.log(detail);
  }



}
