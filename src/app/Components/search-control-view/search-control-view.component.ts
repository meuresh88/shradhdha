import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {Router} from "@angular/router";
import {URLS} from "../../app.module";

@Component({
  selector: 'search-view',
  templateUrl: 'search-control-view.component.html',
  styleUrls: ['search-control-view.component.css']
})
export class SearchControlViewComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  @Input('searchControllerSettings')
  settings:any; //{isParent:true}

  @Output('searchParameter')
  searchPara:EventEmitter<String> = new EventEmitter<String>();

  searchWord:String; // searching word parameter

  searchFor(searchWord:String):void{
    this.searchPara.emit(searchWord);
  }

  addNew():void{
    var bool = window.location.pathname.indexOf(URLS.parentView)>0;
    let link = ['/add', bool];
    this.router.navigate(link);
  }


}
