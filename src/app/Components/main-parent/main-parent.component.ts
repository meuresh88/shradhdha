import { Component, OnInit } from '@angular/core';
import {Parent} from "../../Objects/parent";
import {SearchControlViewComponent} from "../search-control-view/search-control-view.component";
import {ListViewComponent} from "../list-view/list-view.component";
import {ListPageData} from "../../Objects/ListPageData";

@Component({
  selector: 'app-main-parent',
  templateUrl: 'main-parent.component.html',
  styleUrls: ['main-parent.component.css'],
 })
export class MainParentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.tempParent   = {name:"Sunil",address:"844/5,Nawala,Gampola",ammount:4500,email:"temp@gmail.com",phone:"0718266496",regNum:"dupmRegNo"}
    this.parentList = [this.tempParent];
    this.tempPageData= {list:this.parentList,currentPageNumber:1,maxPageNumber:1};
  }

  tempPageData:ListPageData;
  tempParent:Parent;

  parentList:any[];

}
