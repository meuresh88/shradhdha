import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {ListPageData} from "../../Objects/ListPageData";
import { Router} from "@angular/router";
// import {LocalStorage, SessionStorage} from "angular2-localstorage/WebStorage";

@Component({
  selector: 'list-view',
  templateUrl: 'list-view.component.html',
  styleUrls: ['list-view.component.css'],

})
export class ListViewComponent implements OnInit {

  constructor(private router:Router) {

  }
  // @LocalStorage() public lastSelectedObject:Object;
  ngOnInit() {
  }

  @Output('listViewItem')
  selectedeItem:EventEmitter<any> = new EventEmitter<any>();

  @Output('listViewPageNumber')
  pageNumber:EventEmitter<number> = new EventEmitter<number>();

  @Input('listViewSettings')
  settings:any;//{isParent:true}

  @Input('listViewParentStdList')
  pageData:ListPageData;//{list:[], maxPageNumber:n, currentPageNumber:[1,n]==> n= maxPageNumber}

  selectItem(selectedItem):void{
    this.selectedeItem.emit(selectedItem);
  }

  nevigate(pageNumber):void{
    if(pageNumber > 0 || pageNumber<= this.pageData.maxPageNumber){
      this.pageNumber.emit(pageNumber);
    }
  }

  gotoInforView(item,isParent){
    let link = ['/edit', item.regNum,isParent];
    this.router.navigate(link);
    // this.saveListObjectToLocalHost(item);
  }

  // saveListObjectToLocalHost(selectedObject):void{
  //   this.lastSelectedObject=selectedObject;
  // }
}
